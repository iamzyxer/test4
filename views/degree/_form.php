<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;

/* @var $this yii\web\View */
/* @var $model app\models\Degree */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="degree-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'degreeValues')->widget(MultipleInput::className(), [
        'max' => 4,
        'allowEmptyList' => true,
        'columns' => [
            [
                'name'  => 'id',
                'type'  => 'hiddenInput',
            ],
            [
                'name'  => 'locale',
                'type'  => 'dropDownList',
                'title' => 'Локаль',
                'defaultValue' => 'en',
                'items' => [
                    'en' => 'English',
                    'de' => 'Deutsch'
                ]
            ],

            [
                'name'  => 'title',
                'title' => 'Перевод',
                'enableError' => true,
                'options' => [
                    'class' => 'input-priority'
                ]
            ]
        ]
    ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
