<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            'id',
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'text',
                'label' => 'Специалист',
                'content' => function($data) {
                    /** @var app\models\Attendance $data */
                    return $data->specialization->title . '<br><small>' . $data->degree->title . '</small>';
                },
                'contentOptions' =>['class' => 'text-muted'],
            ],

            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'patientSurname',
                'format' => 'text',
                'label' => 'Пациент',
                'content' => function($data) {
                        /** @var app\models\Attendance $data */
                        return $data->patientSurname . ' ' . $data->patientFirstName . ' ' . $data->patientMiddleName;
                    },
                'contentOptions' =>['class' => 'text-muted'],
            ],

            'patientEmail:email',

            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'dateSpecified',
                'format' => ['date', 'php:d.m.Y H:i'],
                'label' => 'Дата приёма',
            ],

            [
                'label' => 'Оплата',
                'attribute' => 'isPaid',
                'format' => 'raw',
                'value' => function($data){
                        return Html::a(
                            ($data->isPaid) ? 'Оплачено' : 'Не оплачено',
                            ['attendance/paid', 'id' => $data->id],
                            [
                                'title' => 'Сменить флаг оплаты',
                                'onclick' => "return confirm('Вы уверены?')",
                            ]
                        );
                    }
            ],
        ],
    ]); ?>
</div>
