<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SpecializationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Справочник, специализации';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialization-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Specialization', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            [
                'class' => 'yii\grid\DataColumn',
                'attribute' => 'title',
                'format' => 'text',
                'label' => 'Название',
            ],
            [
                'class' => 'yii\grid\DataColumn',
                'format' => 'text',
                'label' => 'Перевод',
                'content' => function($data) {
                        $v = [];
                        /** @var app\models\Specialization $data */
                        $res = $data->getSpecializationValues()->all();
                        foreach ($res as $i) {
                            $v[] = "{$i->locale}: {$i->title}";
                        }
                        return empty($v) ? "&mdash;" : implode(', ', $v);
                    },
                'contentOptions' =>[ 'class' => 'text-muted'],
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Действия',
                'headerOptions' => ['width' => '80'],
                'template' => '{update} {delete}',
                'contentOptions' => ['class' => 'text-right text-nowrap'],
            ],
        ],
    ]); ?>
</div>
