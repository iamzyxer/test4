<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specialization".
 *
 * @property string $id
 * @property string $title
 *
 * @property Attendance[] $attendances
 * @property SpecializationValue[] $specializationValues
 */
class Specialization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specialization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendances()
    {
        return $this->hasMany(Attendance::className(), ['specializationId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecializationValues()
    {
        return $this->hasMany(SpecializationValue::className(), ['specializationId' => 'id']);
    }
}
