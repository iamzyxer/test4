<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "degreeValue".
 *
 * @property string $id
 * @property string $degreeId
 * @property string $title
 * @property string $locale
 *
 * @property Degree $degree
 */
class DegreeValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'degreeValue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['degreeId', 'title', 'locale'], 'required'],
            [['degreeId'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['locale'], 'string', 'max' => 10],
            [['degreeId'], 'exist', 'skipOnError' => true, 'targetClass' => Specialization::className(), 'targetAttribute' => ['degreeId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'degreeId' => 'Degree ID',
            'title' => 'Название',
            'locale' => 'Локаль',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegree()
    {
        return $this->hasOne(Degree::className(), ['id' => 'degreeId']);
    }
}
