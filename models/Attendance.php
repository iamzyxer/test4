<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "attendance".
 *
 * @property string $id
 * @property string $specializationId
 * @property string $degreeId
 * @property string $patientFirstName
 * @property string $patientSurname
 * @property string $patientMiddleName
 * @property string $patientEmail
 * @property int $isPaid
 * @property string $dateSpecified
 *
 * @property Degree $degree
 * @property Specialization $specialization
 */
class Attendance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specializationId', 'degreeId', 'patientFirstName', 'patientSurname', 'patientEmail', 'dateSpecified'], 'required'],
            [['specializationId', 'degreeId', 'isPaid'], 'integer'],

            [['dateSpecified'], 'compare', 'compareValue' => date('Y-m-d H:i:s'), 'operator'=>'>='],
            [['patientFirstName', 'patientSurname', 'patientMiddleName', 'patientEmail'], 'string', 'max' => 255],
            [['dateSpecified'], 'unique'],
            [['degreeId'], 'exist', 'skipOnError' => true, 'targetClass' => Degree::className(), 'targetAttribute' => ['degreeId' => 'id']],
            [['specializationId'], 'exist', 'skipOnError' => true, 'targetClass' => Specialization::className(), 'targetAttribute' => ['specializationId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Номер заявки',
            'specializationId' => 'Специализация',
            'degreeId' => 'Научная',
            'patientFirstName' => 'Имя',
            'patientSurname' => 'Фамилия',
            'patientMiddleName' => 'Отчество',
            'patientEmail' => 'Email',
            'isPaid' => 'Оплачено',
            'dateSpecified' => 'Дата приёма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegree()
    {
        return $this->hasOne(Degree::className(), ['id' => 'degreeId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialization()
    {
        return $this->hasOne(Specialization::className(), ['id' => 'specializationId']);
    }
}
