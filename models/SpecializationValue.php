<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specializationValue".
 *
 * @property string $id
 * @property string $specializationId
 * @property string $title
 * @property string $locale
 *
 * @property Specialization $specialization
 */
class SpecializationValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specializationValue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specializationId', 'title', 'locale'], 'required'],
            [['specializationId'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['locale'], 'string', 'max' => 10],
            [['specializationId'], 'exist', 'skipOnError' => true, 'targetClass' => Specialization::className(), 'targetAttribute' => ['specializationId' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'specializationId' => 'Specialization ID',
            'title' => 'Название',
            'locale' => 'Локаль',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialization()
    {
        return $this->hasOne(Specialization::className(), ['id' => 'specializationId']);
    }
}
