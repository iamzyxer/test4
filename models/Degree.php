<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "degree".
 *
 * @property string $id
 * @property string $title
 *
 * @property Attendance[] $attendances
 * @property DegreeValue[] $degreeValues
 */
class Degree extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'degree';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
        ];


    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'degreeValues' => 'Переводы'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendances()
    {
        return $this->hasMany(Attendance::className(), ['degreeId' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDegreeValues()
    {
        return $this->hasMany(DegreeValue::className(), ['degreeId' => 'id']);
    }
}
