# Задача #

Написать backend для хранения заявок на прием к врачу.

Заявка должна содержать поля: Ф, И, О, email,
интересующая специализация врача (справочник: терапевт, хирург, педиатр),
научная степень врача (справочник: специалист, кандидат наук, доктор наук),
описание проблемы,
желаемая дата приема (должна быть больше текущей),
флаг "заявка оплачена" (по умолчанию = false).

Необходимо организовать админ-панель и REST API для работы приложения.

Ожидается, что:
- Справочники будут заполняться из админки и будут доступны для получения через API.
- Заявки будут создаваться через API, просматриваться через админку, а так же в админке будет возможность выставления флага "заявка оплачена".

**************

Тех требования: Yii2 (basic template), Postgresql. В комплекте положил Vagrantfile, советую использовать :)

Доступы к БД:
user: ubuntu, password: ubuntu, dbname: test_mission.

Ожидаемое время выполнения - 2 часа, то есть сделано должно быть аккуратно, но "упарываться" не стоит)

**************

Дополнительное задание (+ 500 к карме):

- Добавить возможность вбивать из аминки перевод для пунктов справочников (ru/en), хранение переводов сделать универсальным для возможности быстро подключать туда другие типовые справочники (id, значение)
- Сделать возможность указания необходимого языка при запросе справочника через API


Dump:

```pgsql
CREATE TABLE "attendance" (
    "id" integer  NOT NULL,
    "specializationId" integer  NOT NULL,
    "degreeId" integer  NOT NULL,
    "patientFirstName" varchar(510) NOT NULL,
    "patientSurname" varchar(510) NOT NULL,
    "patientMiddleName" varchar(510) DEFAULT NULL,
    "patientEmail" varchar(510) NOT NULL,
    "isPaid" int4 NOT NULL DEFAULT '0',
    "dateSpecified" timestamp with time zone NOT NULL,
    PRIMARY KEY ("id"),
    UNIQUE ("dateSpecified")
);
CREATE TABLE "degree" (
    "id" integer  NOT NULL,
    "title" varchar(510) NOT NULL,
    PRIMARY KEY ("id")
);
CREATE TABLE "degreeValue" (
    "id" integer  NOT NULL,
    "degreeId" integer  NOT NULL,
    "title" varchar(510) NOT NULL,
    "locale" varchar(20) NOT NULL,
    PRIMARY KEY ("id")
);
CREATE TABLE "specialization" (
    "id" integer  NOT NULL,
    "title" varchar(510) NOT NULL,
    PRIMARY KEY ("id")
);
CREATE TABLE "specializationValue" (
    "id" integer  NOT NULL,
    "specializationId" integer  NOT NULL,
    "title" varchar(510) NOT NULL,
    "locale" varchar(20) NOT NULL,
    PRIMARY KEY ("id")
);
INSERT INTO "degree" VALUES (1,'Cпециалист'),(2,'Доктор наук'),(3,'Кандидат наук'),(4,'Новая специальность'),(5,'Специалист');
INSERT INTO "specialization" VALUES (1,'Cпециалист'),(2,'Лор'),(3,'Педиатр'),(4,'Терапевт'),(5,'Хирург');

-- Typecasts --
ALTER TABLE "attendance" ALTER COLUMN "isPaid" DROP DEFAULT, ALTER COLUMN "isPaid" TYPE boolean USING CAST("isPaid" as boolean);

-- Foreign keys --
ALTER TABLE "attendance" ADD CONSTRAINT "attendance_ibfk_1" FOREIGN KEY ("specializationId") REFERENCES "specialization" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "attendance" ("specializationId");
ALTER TABLE "attendance" ADD CONSTRAINT "attendance_ibfk_2" FOREIGN KEY ("degreeId") REFERENCES "degree" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "attendance" ("degreeId");
ALTER TABLE "degreeValue" ADD CONSTRAINT "degreeValue_ibfk_1" FOREIGN KEY ("degreeId") REFERENCES "degree" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "degreeValue" ("degreeId");
ALTER TABLE "specializationValue" ADD CONSTRAINT "specializationValue_ibfk_1" FOREIGN KEY ("specializationId") REFERENCES "specialization" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX ON "specializationValue" ("specializationId");

-- Sequences --
CREATE SEQUENCE attendance_id_seq;
SELECT setval('attendance_id_seq', max(id)) FROM attendance;
ALTER TABLE "attendance" ALTER COLUMN "id" SET DEFAULT nextval('attendance_id_seq');
CREATE SEQUENCE degree_id_seq;
SELECT setval('degree_id_seq', max(id)) FROM degree;
ALTER TABLE "degree" ALTER COLUMN "id" SET DEFAULT nextval('degree_id_seq');
CREATE SEQUENCE degreeValue_id_seq;
SELECT setval('degreeValue_id_seq', max(id)) FROM degreeValue;
ALTER TABLE "degreeValue" ALTER COLUMN "id" SET DEFAULT nextval('degreeValue_id_seq');
CREATE SEQUENCE specialization_id_seq;
SELECT setval('specialization_id_seq', max(id)) FROM specialization;
ALTER TABLE "specialization" ALTER COLUMN "id" SET DEFAULT nextval('specialization_id_seq');
CREATE SEQUENCE specializationValue_id_seq;
SELECT setval('specializationValue_id_seq', max(id)) FROM specializationValue;
ALTER TABLE "specializationValue" ALTER COLUMN "id" SET DEFAULT nextval('specializationValue_id_seq');
```

# Установка #

1. git clone https://iamzyxer@bitbucket.org/iamzyxer/test4.git
2. composet install
4. Создать БД
5. Настроить в конфиге соединение с БД config/db.php
6. Настроить права доступа для директорий Yii2 ( runtime, web/assets )