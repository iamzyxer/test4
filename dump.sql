-- MySQL dump 10.13  Distrib 5.5.55, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: db_test4
-- ------------------------------------------------------
-- Server version	5.5.55-0+deb8u1
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO,POSTGRESQL' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table "attendance"
--

DROP TABLE IF EXISTS "attendance";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "attendance" (
  "id" int(11) unsigned NOT NULL,
  "specializationId" int(11) unsigned NOT NULL,
  "degreeId" int(11) unsigned NOT NULL,
  "patientFirstName" varchar(255) NOT NULL,
  "patientSurname" varchar(255) NOT NULL,
  "patientMiddleName" varchar(255) DEFAULT NULL,
  "patientEmail" varchar(255) NOT NULL,
  "isPaid" tinyint(1) NOT NULL DEFAULT '0',
  "dateSpecified" datetime NOT NULL,
  PRIMARY KEY ("id"),
  UNIQUE KEY "dateSpecified" ("dateSpecified"),
  KEY "degreeId" ("degreeId"),
  KEY "specializationId" ("specializationId"),
  KEY "isPaid" ("isPaid"),
  CONSTRAINT "attendance_ibfk_1" FOREIGN KEY ("specializationId") REFERENCES "specialization" ("id"),
  CONSTRAINT "attendance_ibfk_2" FOREIGN KEY ("degreeId") REFERENCES "degree" ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "attendance"
--

LOCK TABLES "attendance" WRITE;
/*!40000 ALTER TABLE "attendance" DISABLE KEYS */;
INSERT INTO "attendance" VALUES (4,1,2,'Антон','Янчук','Алексеевич','yaa@binarika.ru',0,'2018-06-15 00:00:00'),(5,1,2,'Антон','Янчук','Алексеевич','yaa@binarika.ru',0,'2018-08-15 00:00:00'),(6,1,2,'Антон','Янчук','Алексеевич','yaa@binarika.ru',0,'2018-01-15 00:00:00');
/*!40000 ALTER TABLE "attendance" ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table "degree"
--

DROP TABLE IF EXISTS "degree";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "degree" (
  "id" int(11) unsigned NOT NULL,
  "title" varchar(255) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "title" ("title")
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "degree"
--

LOCK TABLES "degree" WRITE;
/*!40000 ALTER TABLE "degree" DISABLE KEYS */;
INSERT INTO "degree" VALUES (1,'Cпециалист'),(4,'Доктор наук'),(2,'Кандидат наук'),(11,'Новая специальность'),(3,'Специалист');
/*!40000 ALTER TABLE "degree" ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table "degreeValue"
--

DROP TABLE IF EXISTS "degreeValue";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "degreeValue" (
  "id" int(11) unsigned NOT NULL,
  "degreeId" int(11) unsigned NOT NULL,
  "title" varchar(255) NOT NULL,
  "locale" varchar(10) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "degreeId" ("degreeId"),
  KEY "locale" ("locale"),
  KEY "title" ("title"),
  CONSTRAINT "degreeValue_ibfk_1" FOREIGN KEY ("degreeId") REFERENCES "degree" ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "degreeValue"
--

LOCK TABLES "degreeValue" WRITE;
/*!40000 ALTER TABLE "degreeValue" DISABLE KEYS */;
INSERT INTO "degreeValue" VALUES (3,1,'Specialist2','en'),(4,1,'Spezialist22','de'),(5,2,'fsdfsadfsad','en'),(6,2,'555g','de'),(7,11,'fsdfsdf','en');
/*!40000 ALTER TABLE "degreeValue" ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table "specialization"
--

DROP TABLE IF EXISTS "specialization";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "specialization" (
  "id" int(11) unsigned NOT NULL,
  "title" varchar(255) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "title" ("title")
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "specialization"
--

LOCK TABLES "specialization" WRITE;
/*!40000 ALTER TABLE "specialization" DISABLE KEYS */;
INSERT INTO "specialization" VALUES (3,'Cпециалист'),(7,'Лор'),(6,'Педиатр'),(1,'Терапевт'),(2,'Хирург');
/*!40000 ALTER TABLE "specialization" ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table "specializationValue"
--

DROP TABLE IF EXISTS "specializationValue";
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE "specializationValue" (
  "id" int(11) unsigned NOT NULL,
  "specializationId" int(11) unsigned NOT NULL,
  "title" varchar(255) NOT NULL,
  "locale" varchar(10) NOT NULL,
  PRIMARY KEY ("id"),
  KEY "title" ("title"),
  KEY "specializationId" ("specializationId"),
  KEY "locale" ("locale"),
  CONSTRAINT "specializationValue_ibfk_1" FOREIGN KEY ("specializationId") REFERENCES "specialization" ("id")
);
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table "specializationValue"
--

LOCK TABLES "specializationValue" WRITE;
/*!40000 ALTER TABLE "specializationValue" DISABLE KEYS */;
INSERT INTO "specializationValue" VALUES (1,3,'555d','en'),(2,3,'fsdfsdf','de'),(3,7,'LOR','en');
/*!40000 ALTER TABLE "specializationValue" ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-09 13:58:39
