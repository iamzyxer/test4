<?php

namespace app\controllers\api;

use yii\rest\ActiveController;

class DegreeController extends ActiveController
{
    public $modelClass = 'app\models\Degree';

    public function actions()
    {
        $actions = parent::actions();
        
        return $actions;
    }
}
