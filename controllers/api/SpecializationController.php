<?php

namespace app\controllers\api;

use yii\rest\ActiveController;

class SpecializationController extends ActiveController
{
    public $modelClass = 'app\models\Specialization';
    public function actions()
    {
        $actions = parent::actions();
        unset($actions['delete'], $actions['update'], $actions['create']);
        return $actions;
    }
}
