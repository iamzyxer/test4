<?php

namespace app\controllers\api;

use yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\data\DataFilter;

class AttendanceController extends ActiveController
{
    public $modelClass = 'app\models\Attendance';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['delete'], $actions['update']);
        return $actions;
    }
}
