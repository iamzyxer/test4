<?php

namespace app\controllers;

use Yii;
use app\models\Degree;
use app\models\DegreeValue;
use app\models\DegreeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * DegreeController implements the CRUD actions for Degree model.
 */
class DegreeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Degree models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DegreeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Degree model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Degree model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Degree();
        if (Yii::$app->request->post()) {
            $model->title = ArrayHelper::getValue(Yii::$app->request->post(), 'Degree.title');

            if ($model->validate()) {
                $model->save();
                $this->addValues($model, ArrayHelper::getValue(Yii::$app->request->post(), 'Degree.degreeValues'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Degree model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->post()) {
            $model->title = ArrayHelper::getValue(Yii::$app->request->post(), 'Degree.title');

            if ($model->validate()) {
                $model->save();

                $this->addValues($model, ArrayHelper::getValue(Yii::$app->request->post(), 'Degree.degreeValues'));

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }


        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Degree model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Degree model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Degree the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Degree::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * @param Degree $model
     * @param array $vals
     */
    protected function addValues(Degree $model, $vals = [])
    {
        if (!empty($vals)) {
            foreach ($vals as $v) {

                if (empty($v['id'])) {
                    $m = new DegreeValue();
                } else {
                    $m = DegreeValue::findOne($v['id']);
                }

                if (empty($m) || empty($v['locale']) || empty($v['title'])) continue;

                $m->degreeId = $model->id;
                $m->locale = $v['locale'];
                $m->title = $v['title'];
                $m->save();
            }
        }
    }
}
